package MongoDBCrudExercise;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        MongoDbCrud mongoDbCrud = new MongoDbCrud();
        mongoDbCrud.MakeConnection();
        mongoDbCrud.GetDatabase();
        mongoDbCrud.CreateCollection();
     //   mongoDbCrud.CreateDocument();
        mongoDbCrud.ReadCollection();
        mongoDbCrud.UpdateCollection();
     //   mongoDbCrud.DeleteDocument();
        mongoDbCrud.ReadCollection();
        mongoDbCrud.CloseConnection();
    }
}
