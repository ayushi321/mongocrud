package MongoDBCrudExercise;


import com.mongodb.MongoClient;

public class MongoDbConnection {

    protected MongoClient mongoClient;

    public void MakeConnection(){

        mongoClient = new MongoClient("10.246.0.108",27017);
        System.out.println("Successfully Connected to mongodb");

    }

    public void CloseConnection(){
        mongoClient.close();
        System.out.println("Connection is closed to mongodb");
    }
}
