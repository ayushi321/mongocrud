package MongoDBCrudExercise;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.util.Iterator;

public class MongoDbCrud extends MongoDbConnection {

    private MongoDatabase database;
    private MongoCollection<Document> collection;

    public void GetDatabase(){
         database = mongoClient.getDatabase("project");
        System.out.println("Database project is in use : ");
    }
    public void CreateCollection(){
        collection = database.getCollection("Student");
        System.out.println("Collection is created: ");
    }
    public void CreateDocument(){
        Document document = new Document("student_id",103)
                .append("student_name","Rim")
                .append("gender","Female")
                .append("dob","07-03-98")
                .append("address","Model town Delhi");
        collection.insertOne(document);

    }
    public void ReadCollection(){
        System.out.println("Reading Document");
        FindIterable<Document> mydoc = collection.find();
        Iterator itr = mydoc.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
    }
    public void UpdateCollection(){
        collection.updateOne(Filters.eq("student_name","Roy"), Updates.set("student_id",101));

        System.out.println("Document updated");
    }
    public void DeleteDocument(){
        collection.deleteOne(Filters.eq("student_id",102));
        System.out.println("Document deleted");
    }
}
